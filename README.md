# Weather-related challenges and accessibility calculation for offshore wind developments: a feasibility case study from NW Scotland

## Accompaging notebook

This notebook contains the codes used for the analysis propsoed in the paper 'Weather-related challenges and accessibility calculation for offshore wind developments: a feasibility case study from NW Scotland.' (de Benedicts et al. 2023).

## Running the notebook

You can clone and run the notebook localy, by installing the packages indicated in the file 'requirements.txt'. For example, using `pip`:

```bash
pip install pip-sync
pip-sync
```

It is also possible to just visualize the results using [Nbviewer](https://nbviewer.org/urls/gitlab.ifremer.fr/nr22372/accessibility-calculation-OW/-/raw/main/Weather%20Windows.ipynb) or even running it on binder [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.ifremer.fr%2Fnr22372%2Faccessibility-calculation-OW/HEAD)

